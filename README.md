<h1 align="center">nanochoo</h1>

<div align="center">
  ½🚂🚋🚋🚋🚋
</div>
<div align="center">
  <strong><a href='https://github.com/choojs/choo'>choo</a> but half the size</strong>
</div>
<div align="center">
  A <code>2kb</code> framework for creating sturdy frontend applications on a diet
</div>

<br />

<div align="center">
  <sub>Fork of <a href='https://github.com/nanaian/nanochoo'>nanochoo</a>. Built with ❤︎ by
  <a href="https://twitter.com/yoshuawuyts">Yoshua Wuyts</a> and
  <a href="https://github.com/heyitsmeuralex/nanochoo/graphs/contributors">
    contributors
  </a>
</div>

## Key differences

* `choo/html` removed - use [nanohtml](https://wzrd.in/standalone/nanohtml@latest) (`npm install nanohtml`) directly
* Removed router:
  * `choo()` no longer takes an `opts` argument
  * `choo.route(location, handler)` replaced by `choo._view = handler`
  * `pushState`, `popState`, `replaceState` events removed
  * nanorouter, scroll-to-anchor, nanolocation, nanoquery no longer dependencies
* rename `DOMCONTENTLOADED` to `ONLOAD`, `DOMTITLECHANGE` to `TITLE`, and `NAVIGATE` to `GO`
* replace `choo.use` and document-ready dependency with `choo.ready` function 
* `choo.toString` is removed

See [choo](https://github.com/choojs/choo) for documentation - just ignore
routing-related things, use `choo._view` over `choo.route`, and you'll be fine.

## Example
```js
var html = require('nanohtml');
var choo = require('nanochoo');

var app = choo();
app.ready(() => countStore(app)); // instead of use!
app._view = mainView; // instead of route!
app.mount('body');

function mainView (state, emit) {
  return html`
    <body>
      <h1>count is ${state.count}</h1>
      <button onclick=${() => emit('increment', 1)}>increment!</button>
    </body>
  `;
}

function countStore (app) {
  const { state, emitter } = app;
  state.count = 0;
  emitter.on('increment', function (count) {
    state.count += count;
    emitter.emit('render');
  });
}
```

## License
[MIT](https://tldrlegal.com/license/mit-license). See [choo](https://github.com/choojs/choo)
